from allImports import *
from app.users  import *
from app.switch import switch
from flask import send_file
import os
import datetime
################################################################################
@app.route("/upload/", defaults={'cmd': 0}, methods = ["GET", "POST"])
@app.route("/upload/<cmd>", methods = ["GET", "POST"])
def upload(cmd):
  if request.method == "POST":
    app.logger.info("Attempting to upload file.")
    file = request.files['file']
    
    get_time = datetime.datetime.now()
    time_stamp = get_time.strftime("%Y-%m-%d %H:%M")
    filename = ('UploadedFile' + str(time_stamp) + "." + str(file.filename.split(".").pop()))
    complete_path = ("app/static/files/uploads/" + str(filename)).replace(" ", "")
    file.save(complete_path)
    update_last_modified = Files.update(last_modified=time_stamp).where(Files.uid==cmd)
    update_last_modified.execute()
    update_file_path = Files.update(path=complete_path).where(Files.uid==cmd)
    update_file_path.execute()
    app.logger.info("Upload file.")
  Stuff = Files.select()
  return render_template("submission.html", cfg = cfg, files = Stuff)
  
################################################################################

@app.route("/upload/<cmd>/download", methods = ["POST","GET"])
def filedownload(cmd):
  try:
    file_path = Files.select(Files.path).where(Files.uid == cmd).get()
    return send_file(file_path, as_attachment=True)
  
  except Exception,e:
    app.logger.info("{0} attempting to upload file.".format(str(e)))
    message = "An error occured during the download process."
    return render_template("error.html",
                              cfg                   = cfg,
                              message               = message
                            )

@app.route("/upload/<cmd>/delete", methods = ["POST"])
def filedelete(cmd):
  user_name = "heggens"
  try:
    file_path = Files.select(Files.path).where(Files.uid == cmd).get()
    os.remove(file_path)
    delete_filePath = Files.update(filePath=None).where(Files.uid==cmd)
    delete_filePath.execute()
    #RECORD THE CHANGE
    get_time = datetime.datetime.now()
    time_stamp = get_time.strftime("%Y-%m-%d %I:%M")
    
    update_last_modified = Files.update(last_modified=time_stamp).where(Files.uid==cmd)
    update_last_modified.execute()
    return redirect('/upload/',code=302)
  except Exception,e:
    app.logger.info("{0} attempting to delete a syllabus.".format(str(e)))
    message = "An error occured during the delete process of the file."
    return render_template("error.html",
                              cfg                   = cfg,
                              message               = message
                            )