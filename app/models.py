from peewee import *
import os
#from allImports import *   #Don't believe this import is needed for this file
# Create a database
from app.loadConfig import *

cfg = load_config('app/config.yaml')
db = SqliteDatabase(cfg['databases']['dev'])

# Now, define a class representing the fields of our database.
class BaseModel(Model):
  class Meta:
    database = db

class Stuffs  (BaseModel):
  uid         = PrimaryKeyField()
  name        = TextField()
  value       = TextField()
  count       = IntegerField()

class Hierarchy(BaseModel):
  uid         = PrimaryKeyField()
  type        = TextField()

class Entries (BaseModel):
  uid         = PrimaryKeyField()
  title       = TextField()
  text        = TextField()
  item        = ForeignKeyField(Stuffs, related_name='entries')

class Files (BaseModel):
  uid         = PrimaryKeyField()
  path        = TextField()
  last_modified = TextField()
# The bscr database

# When adding new tables to the DB, add a new class here (also add 
# to the config.yaml file)
###########################CLASSES#############################
#Class Names        -> Camel Case                                         Ex. Semester
#Primary Keys       -> All Caps                                           Ex. SEID
#ForeignKeyField    -> Same Name as Key being related to                  Ex. Divisions.DID = Programs.DID
#Variables in Class -> Mixed Case                                         Ex. filePath


######## CLASSES WITH NO FOREIGN KEY FIELD ########
class Semesters (BaseModel):
  SEID          = PrimaryKeyField()
  year          = IntegerField()
  term          = CharField()
  current       = IntegerField(default = 1)
 
class Divisions (BaseModel):
  DID           = PrimaryKeyField()
  name          = CharField()
  
######## CLASSES WITH FOREIGN KEY FIELDS ########
class Programs (BaseModel):
  PID           = PrimaryKeyField()
  name          = CharField()
  DID           = ForeignKeyField(Divisions)
  
class Users (BaseModel):
  UID           = PrimaryKeyField()
  firstName     = CharField()
  lastName      = CharField()
  userName      = CharField()
  email         = CharField()
  password      = CharField()
  admin         = IntegerField(default = 0)
  PID           = ForeignKeyField(Programs,  null = True)
  DID           = ForeignKeyField(Divisions, null = True)
  
class Courses (BaseModel):
  CID           = PrimaryKeyField()
  prefix        = CharField()
  number        = CharField()
  section       = CharField()
  PID           = ForeignKeyField(Programs)
  SEID          = ForeignKeyField(Semesters)
  filePath      = TextField(null = True)
  lastModified  = TextField(null = True)
  
class UsersCourses (BaseModel):
  UCID          = PrimaryKeyField()
  userName      = ForeignKeyField(Users, to_field = "userName")
  CID           = ForeignKeyField(Courses)
