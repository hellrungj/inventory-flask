# IMPORTS
from __future__ import print_function # import the Flask class from the flask module
from allImports import *
from flask import session, flash
from random import randint
import os
import datetime

cfg = load_config('app/config.yaml')

# Define the DB object.
# Takes the filename. We'll pull this out into a config file
# for the actual app. This is OK for testing for now.
db = SqliteDatabase(cfg['databases']['dev'])
SECRET_KEY = 'development key'

# FLASK INIT
#app   = Flask(__name__)
app.config.from_object(__name__)

######################################################
# ROUTE: /
# Action:
# The root route reroutes you to a view of the inventory.
# Specifically, it shows us everything we have
# more than zero of. Note the use of url_for, which essentially
# makes sure we call the function for the handler correctly,
# with the parameters in the URL even!
@app.route('/', methods=["GET"])
def home():
  """ The "home" function asks if the brower has not record of the user being login.
  True: Returns user to login page
  False: Redurects user to the main page of the application
  """
  if not session.get('logged_in'):
    return render_template('login.html')
  else:
    return redirect(url_for('search', cnt = 0), code=302)

#This allows the user to login to the application.
@app.route('/login', methods=['GET', 'POST'])
def login():
    """ The "login" function first set the message to 0 and the error to None.
    Then asks if the request is POST method. If true then the funcation asks if
    there is a User with the same username and password in the current database.
    If there is no username then it sets the error to 'Invalid username' or if there
    is no password then it sets the error to 'Invalid password'. If either username
    or password are found they will add one to the message. After, all that it checks
    for the final time to see if message is equal to two. if this is true then the
    session 'logged_in' will be set to true and a flash message will be send to the
    user notifing them that they are login in which they be will redirect to the main
    page of the appliction. If the does happen that the user is redirected to thee
    login page again and meet with a flash message.  
    """
    message = 0
    error = None
    if request.method == 'POST': 
        if Users.select().where(request.form['username']):
            pass
        else:
            message += 1
            error = 'Invalid username'
        if Users.select().where(request.form['password']):
            pass
        else:
            message += 1
            error = 'Invalid password'
        if message == 2:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('search', cnt = 0), code=302)
    return render_template('login.html', error=error)

#This allows the user to logout of the application
@app.route('/logout')
def logout():
  """ The "Logout" function pops and sets the session logged_in. Then send the user a
  flash message telling them that "You were logged out". After that it redirects the
  user to the login page. 
  """
  session.pop('logged_in', None)
  flash('You were logged out')
  return redirect(url_for('login'))