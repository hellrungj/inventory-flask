from allImports import *
from flask import session, flash, abort
######################################################
# ROUTE: /morethan/<int:count>
# METHODS: GET
# Action:
# Updates the inventory search results to show items
# where we have more than the number given.
@app.route('/search/<int:cnt>')
def search (cnt):
  # We scan all of the items, finding ones that have a 
  # count greater than the value of the variable 'cnt'.
  # response = Item.scan(count__gt = cnt)
  response = Stuffs.select().where(Stuffs.count > cnt)
  entries = Entries.select().join(Stuffs).where(Stuffs.count > cnt)
  # Render the search results, passing the list of items
  # that we found in our scan.
  return render_template('searchresults.html', items = response, entries=entries)

######################################################
# ROUTE: /contains/<name>
# METHODS: POST, GET
# Action:
# This takes a GET URL prefaced with /contains, and searches
# the name fields of the Items for any that contain the last part
# of the URL. This becomes the search results list on the main page.
# The side effect of this interface is that we can bookmark
# searches of the database.
@app.route('/contains/<search_string>', methods = ['POST', 'GET'])
def contains (search_string):
  # If they did a post, then just redirect to the GET version
  # The FORM on the page does a POST, so we handle this here
  # in this way.
  if request.method == 'POST':
    return redirect("/contains/{0}".format(request.form['name']))
  # By redirecting to the GET, we guarantee bookmarkable search results.
  elif request.method == 'GET':
    search_result = Stuffs.select().where(Stuffs.name.contains(search_string))
    entries = Entries.select().join(Stuffs).where(Stuffs.name.contains(search_string))
    return render_template('searchresults.html', items = search_result, entries=entries)
  else:
    # FIXME: Return a proper error message here.
    return "404"

######################################################
# ROUTE: /insert
# METHODS: POST
# Action:
# Does an insert into the database. No fancy URL here,
# because we do not want to accidentally bookmark a URL and
# end up with a way to have too many of one item in the DB.
@app.route('/insert', methods = ['POST'])
def insert ():
  # This only handles POST requests.
  if request.method == 'POST':
    n  = request.form['name']
    v = request.form['value']
    c = int(request.form['count'])
    # Field name = variable
    stuff = Stuffs(name = n, value = v, count = c)
    stuff.save()
    return redirect(url_for('search', cnt = 0), code=302)
  else:
    # FIXME: Properly handle other request types.
    return "404"

################################################################################
#For Editing Items

@app.route('/edit/<int:num>', methods=['POST', 'GET'])
def show_item(num):
  """ The "show_item" function asks if the browers does not have the session
  has the string 'logged_in'. If the browers doesn't have the session string
  then the brower will abort. After it will set a database quary to a varable
  called "item". This quary is selecting a column in the stuffs table that is
  equal to the value of the 'num' varable. Then the funation use the varable item,
  when sends the user to edit tempelete as another paramater. 
  """
  if not session.get('logged_in'):
    abort(401)
  item = Stuffs.select().where(Stuffs.uid == num)
  return render_template('edit.html', entry=item)

@app.route('/editing/<int:num>', methods=['POST'])
def edit_item(num):
  """ The "edit_item" funcation asks if the method used by the brower is POST. Then
  it ask if the session is not "loggin_in". If true then abort. If not then the funcation
  get the information from the form the set them into varables. Then he funcation performs
  a database quary. This database quary grabs the column in the "Stuff" table where the id
  is equal to the value of the "num" varable. Then it sets the values of the column using
  the varable maded from the form data. Then is save the changes to the database and informs
  the user and redirect them to the main page.
  """
  if request.method == 'POST':
    if not session.get('logged_in'):
      abort(401)
    n  = request.form['name']
    v = request.form['value']
    c = request.form['count']
    item = Stuffs.select().where(Stuffs.uid == num).get()
    item.name = n
    item.value = v
    item.count = c
    item.save()
    flash('New entry was successfully posted')
    return redirect(url_for('search', cnt = 0), code=302)

# For Editing Comments:  

@app.route('/comment/edit/<int:num>', methods=['POST', 'GET'])
def show_entry(num):
  if not session.get('logged_in'):
    abort(401)
  entry = Entries.select().where(Entries.uid == num)
  item = Stuffs.entries
  return render_template('comment_edit.html', entry=entry, item=item)

@app.route('/comment/editing/<int:num>', methods=['POST'])
def edit_entry(num):
  if request.method == 'POST':
    if not session.get('logged_in'):
      abort(401)
    T = request.form['title']
    t = request.form['text']
    i = Stuffs.select(Stuffs.uid).where(Stuffs.name == request.form['item_name'])
    entry = Entries.select().where(Entries.uid == num).get()
    entry.title = T
    entry.text = t
    entry.item = i
    entry.save()
    flash('New entry was successfully posted')
    return redirect(url_for('search', cnt = 0), code=302)
    
@app.route('/add', methods=['POST'])    
def add_entry():
  if request.method == 'POST':
    if not session.get('logged_in'):
      abort(401)
    T = request.form['title']
    t = request.form['text']
    i = Stuffs.get(Stuffs.name == request.form['item_name'])
    entry = Entries(title = T, text = t, item = i)
    entry.save()
    flash('New entry was successfully posted')
    return redirect(url_for('search', cnt = 0), code=302)
  else:
  # FIXME: Properly handle other request types.
    return "404"

# Deleting
# Items
@app.route('/delete/<int:num>', methods=['POST', 'GET'])
def delete_item(num):
  if not session.get('logged_in'):
    abort(401)
  item = Stuffs.get(Stuffs.uid == num)
  item.delete_instance()
  return redirect(url_for('search', cnt = 0), code=302)

# Comments
@app.route('/commment/delete/<int:num>', methods=['POST', 'GET'])
def delete_comment(num):
  if not session.get('logged_in'):
    abort(401)
  entry = Entries.get(Entries.uid == num)
  entry.delete_instance()
  return redirect(url_for('search', cnt = 0), code=302)