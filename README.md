# README #

### What is this repository for? ###

* This a demo inventory-flask project originaly started by Matt Jadud. The purpose of this project is to learn python flask with virtualenv and peewee application project for education purposes.  

### How do I get set up? ###

* You will need to run setup.sh and then activate venv using this command ". venv/bin/activate", this will set up your environment. 
* After that want to set and popludate your database. You will do this by running the create_db.py file. 
* Now, you all are ready to run the application. 

### How to run the application? ###

* First you will run the command "". venv/bin/activate" and run the main.py.
* You should see that application is running, now type "localhost:8080" into your brower of choosing and your done.   
* Once running you need to log in. Here the Account information to test the application:  
	* Username: Admin
	* Password: Root  

### Contribution guidelines ###

* Currently this repo has no test 
* Currently this repo has no method of code review

### Who do I talk to? ###

* You can contact me (hellrungj@berea.edu) or Matt Jadud (https://bitbucket.org/jadudm/)